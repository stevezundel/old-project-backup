from json import dumps, loads
from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.views.decorators.csrf import ensure_csrf_cookie
from models import Questions, Assignments, QuestionTags, Answers
from forms import QuestionForm, AssignmentForm, QuestionTagsFormSet, QuestionTagsForm, AdminForm, UserForm, NameForm
from django.http import JsonResponse
from django.core.context_processors import csrf #in django 1.8 this is moved to django.templates.context_processors

title = "Luminary Academy"


def home(request):
    dictionary = request.session
    dictionary['title'] = title
    dictionary['first_name'] = __get_session(request)
    return render(request, 'luminary/templates/menu.html', dictionary)


def introduction(request):
    dictionary = request.session
    dictionary['title'] = title
    dictionary['first_name'] = __get_session(request)
    return render(request, 'luminary/templates/Introduction.html', dictionary)


def critical_thinking(request):
    dictionary = request.session
    dictionary['title'] = title
    dictionary['first_name'] = __get_session(request)
    return render(request, 'luminary/templates/Critical_Thinking.html', dictionary)


def create_assignments(request):
    dictionary = {'title': title, 'assignments': _load_assignments, 'form': AssignmentForm,
                  'first_name': __get_session(request)}
    return render(request, 'luminary/templates/create-assignments.html', dictionary)

@ensure_csrf_cookie
def question_bank(request):
    dictionary = {'title': title, 'assignments': _load_assignments, 'first_name': __get_session(request),
                  'tags_form': QuestionTagsForm()}
    dictionary.update(csrf(request))
    return render(request, 'luminary/templates/questions/questionBank.html', dictionary)


def add_assignment(request):
    if request.method == "POST":
        form = AssignmentForm(request.POST)
        if form.is_valid():
            Assignments.objects.create(due_date=form.cleaned_data['due_date'],
                                       point_value=form.cleaned_data['point_value'],
                                       title=form.cleaned_data['title'],
                                       type=form.cleaned_data['type'], )
        else:
            raise Http404
    return HttpResponse(dumps(_load_assignments()), content_type="application/json")


def view_assignment_list(request):
    assignments = Assignments.objects.all()
    dictionary = {'title': title, 'assignments': assignments}
    return render(request, 'luminary/templates/assignment_list.html', dictionary)


def view_assignment(request):
    dictionary = {}
    return render(request, 'luminary/templates/assignments.html', dictionary)


def _load_assignments():
    assignment_dict = {}
    for assignment in Assignments.objects.all():
        assignment_dict[assignment.title] = assignment.id
    return assignment_dict


def create_assignment(request, num=None):
    if request.method == 'POST':
        question_form = QuestionForm(request.POST)
        current_assignment = Assignments.objects.get(pk=num)
        if question_form.is_valid():
            question = Questions.objects.create(question=question_form.cleaned_data['question'],
                                                answer=question_form.cleaned_data['answer'],
                                                point_value=question_form.cleaned_data['point_value'],
                                                assignment=current_assignment, )
            tags_inline_formset = QuestionTagsFormSet(request.POST, request.FILES, instance=question, )
            if tags_inline_formset.is_valid():
                for Tags in tags_inline_formset:
                    for tag in Tags.cleaned_data['tag'].split(","):
                        question_tag = QuestionTags.objects.create(question=question,
                                                                   tag=tag, )
                        question_tag.save()

    try:
        assignment_name = Assignments.objects.get(pk=num).title
    except Assignments.DoesNotExist:
        assignment_name = None

    question_list = []
    try:
        question_list = _load_questions(num)

    except Questions.DoesNotExist:
        pass

    dictionary = {'title': title, 'assignment': num, "assignment_name": assignment_name, "questions": question_list,
                  'formset': QuestionTagsFormSet, 'form': QuestionForm, 'first_name': __get_session(request)}
    return render(request, 'luminary/templates/create-assignment.html', dictionary)


def user_details(request, user_id=None):
    __check_permissions('staff', request.user)
    if request.method == "POST":
        in_form = AdminForm(request.POST)
        if in_form.is_valid():
            User.objects.filter(pk=user_id).update(email=in_form.cleaned_data['email'],
                                                   username=in_form.cleaned_data['email'],
                                                   is_staff=in_form.cleaned_data['is_staff'], )
    current_user = User.objects.get(pk=user_id)
    out_form = AdminForm(initial={'email': current_user.email, 'is_staff': current_user.is_staff})
    dictionary = {'title': title, 'form': out_form, 'first_name': __get_session(request)}
    return render(request, 'luminary/templates/user.html', dictionary)


def users_list(request):
    __check_permissions('staff', request.user)
    users = User.objects.all()
    all_users = []
    form = UserForm()
    for current_user in users:
        all_users.append(current_user)
    dictionary = {'users': all_users, 'title': title, 'form': form, 'first_name': __get_session(request)}
    return render(request, 'luminary/templates/users.html', dictionary)


def __get_session(request):
    first_name = None
    try:
        email = request.session['email']
        first_name = User.objects.get(email=email).first_name
    except AttributeError:
        pass
    except KeyError:
        pass
    return first_name


def _load_questions(assignment_id):
    questions = []
    for question in Questions.objects.filter(assignment_id=assignment_id):
        questions.append([question.question, question.answer, question.point_value, ]),
    return questions


def __get_user_permissions(user_to_validate):
    permissions = "user"
    if user_to_validate.is_superuser:
        permissions = "superuser"
    elif user_to_validate.is_staff:
        permissions = "staff"
    return permissions


def __check_permissions(required_permissions, user_to_validate):
    permission = __get_user_permissions(user_to_validate)
    if required_permissions == "superuser":
        if permission in "superuser":
            pass
        else:
            raise PermissionDenied
    elif required_permissions == "staff":
        if permission in ('superuser', 'staff'):
            pass
        else:
            raise PermissionDenied
    else:
        pass


def lectures(request, num=None):
    dictionary = request.session
    dictionary['title'] = title
    dictionary['first_name'] = __get_session(request)
    url = 'luminary/templates/lectures/' + num + '.html'
    #the following block is matt practicing with forms
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
    return render(request, url, {'form': form})


def add_question(request):
    data = {
        'b': 3,
    }
    return JsonResponse(data)


def question_list(request):
    data = []
    for question in Questions.objects.all():
        answers = []
        try:
            for answer in Answers.objects.filter(question=question):
                answer_info = {
                    "answer": answer.answer,
                    "correct": answer.correct,
                    "id": answer.id,
                }
                answers.append(answer_info)
        except ObjectDoesNotExist:
            pass

        row = {
            "id": question.id,
            "question": question.question,
            "answers": answers,
        }
        data.append(row)
    return HttpResponse(dumps(data))


def delete_answer_from_question(request):
    if request.method == "DELETE":
        answer = loads(request.GET['answer'])
        Answers.objects.get(pk=answer['id']).delete()


def add_answers_to_question(request):
    if request.method == "POST":
        answer = loads(request.body)['params']['answer']
        question = loads(request.body)['params']['question']
        db_answers = Answers.objects.create(answer=answer, question_id=int(question['id']))
        db_answers.save()
        return HttpResponse(db_answers.id)


def update_answer(request):
    if request.method == "POST":
        answer = loads(request.body)['params']['answer']
        question = loads(request.body)['params']['question']
        db_answers = Answers.objects.create(answer=answer, question_id=int(question['id']))
        db_answers.save()
        return HttpResponse(db_answers.id)


def update_question(request):
    # __check_permissions('staff', request.user)
    if request.method == "POST":
        question_dictionary = loads(request.body)
        question = question_dictionary['question']
        answers = question_dictionary['answers']
        question_type = "Multiple Choice" if len(answers) > 1 else "Word"
        if question_dictionary['id'] == "":
            new_question = Questions.objects.create(question=question, type=question_type)
            new_question.save()
            return HttpResponse(dumps(new_question.id))
        else:
            db_question = Questions.objects.get(id=question_dictionary['id'])
            db_question.question = question_dictionary['question']
            db_question.save()
            try:
                db_answers = Answers.objects.get(question_id=db_question.id)
                db_answers = answers
            except ObjectDoesNotExist:
                db_answers = Answers.objects.create(answer=answers[0], question=db_question)
            db_answers.save()
            return HttpResponse("")


@ensure_csrf_cookie
def answer_questions(request):
    id_number = 1
    dictionary = request.session
    dictionary['title'] = title
    dictionary['first_name'] = __get_session(request)
    dictionary['question'] = __get_question(id_number=id_number)
    dictionary['question_id'] = id_number
    return render(request, 'luminary/templates/lectures/question-example.html', dictionary)


def submit_answers(request):
    if request.method == "POST":
        answer = request.REQUEST['answer']
        question_id = request.REQUEST['question_id']
    else:
        raise Http404
    if __check_answer(question_id, answer):
        return HttpResponse(__get_question(str(int(question_id) + 1))['question'])
    else:
        return HttpResponse


def __get_question(id_number):
    return Questions.objects.filter(id=id_number).values('question').first()


def __check_answer(question_id, user_answer):
    correct_answer = Answers.objects.get(question_id=question_id)
    return user_answer == str(correct_answer.answer)

