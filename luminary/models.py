from django.db import models
# Matt's test models


class OperationalDefinitions(models.Model):
    # student = models.ForeignKey(Questions)
    question1 = models.TextField(null=False)
    question2 = models.TextField(null=False)
    question3 = models.TextField(null=False)

# Create your models here.


class Questions(models.Model):
    question = models.TextField(null=False)
    image = models.ImageField()
    MC = 'Multiple Choice'
    WORD = 'Word'
    DECIMAL = 'Decimal'
    TYPE = (
        (MC, 'Multiple Choice'),
        (WORD, 'Word'),
        (DECIMAL, 'Decimal'),
    )
    type = models.CharField(max_length=15,
                            choices=TYPE,
                            default=MC)


class Answers(models.Model):
    answer = models.TextField(null=False)
    question = models.ForeignKey(Questions)
    correct = models.BooleanField(default=True)
    image = models.ImageField()


class QuestionTags(models.Model):
    question = models.ForeignKey(Questions)
    tag = models.TextField(max_length=40)


class Assignments(models.Model):
    due_date = models.DateTimeField()
    point_value = models.DecimalField(null=False, max_digits=3, decimal_places=2)
    title = models.TextField(max_length=50)
    TEST = 'Test'
    HOMEWORK = 'HOMEWORK'
    TYPE = (
        (TEST, 'Test'),
        (HOMEWORK, 'Homework'),
    )
    type = models.CharField(max_length=8,
                            choices=TYPE,
                            default=HOMEWORK)


class AssignmentQuestions(models.Model):
    assignment = models.ForeignKey(Assignments)
    question = models.ForeignKey(Questions)


class Students(models.Model):
    student_first_name = models.TextField()
    student_last_name = models.TextField()
    student_id = models.IntegerField()


class StudentQuestionsPassed(models.Model):
    student = models.ForeignKey(Students)
    question = models.ForeignKey(Questions)
    passed = models.BooleanField(default=False)


class Grades(models.Model):
    grade = models.DecimalField(max_digits=3, decimal_places=2)
    student = models.ForeignKey("Students")
    assignment = models.ForeignKey("Assignments")
