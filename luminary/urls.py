from django.conf.urls import patterns, include, url
from luminary.views import home, \
    create_assignments, \
    add_assignment, \
    create_assignment, \
    user_details, \
    users_list, \
    view_assignment_list, \
    view_assignment, \
    introduction, \
    critical_thinking, \
    question_bank, \
    lectures, \
    add_question, \
    question_list, \
    delete_answer_from_question, \
    add_answers_to_question, \
    update_question, \
    update_answer, \
    answer_questions, \
    submit_answers

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       url(regex=r'^$', view=home, name='home_url'),
                       url(regex=r'^(?i)login/', view=include('login.urls'), name='login_url'),
                       url(r'^(?i)assignments/', view_assignment_list),
                       url(r'^(?i)assignments/(\d+)/', view_assignment),
                       url(r'^(?i)create_assignments/', create_assignments),
                       url(r'^(?i)create_assignment/(\d+)/', create_assignment),
                       url(r'^(?i)lectures/(?P<value>\d+\.\d{2})/$', lectures),
                       url(r'^(?i)add_assignment/', add_assignment),
                       url(r'^(?i)question_bank/', question_bank),
                       url(r'^(?i)users/', users_list),
                       url(r'^(?i)user/(\d+)/', user_details),
                       url(r'^(?i)Introduction/', introduction),
                       url(r'^(?i)critical_thinking/', critical_thinking),
                       url(r'^(?i)Question/', add_question),
                       url(r'^(?i)QuestionList/', question_list),
                       url(r'^(?i)DeleteAnswer/', delete_answer_from_question),
                       url(r'^(?i)UpdateQuestion/', update_question),
                       url(r'^(?i)UpdateAnswer/', update_answer),
                       url(r'^(?i)AddAnswersToQuestion/', add_answers_to_question),

                       url(r'^(?i)lectures/question-example/', answer_questions),
                       url(r'^(?i)lectures/submit-question/', submit_answers),
)
