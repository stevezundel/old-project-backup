from django import forms
from django.forms.models import inlineformset_factory
from django.contrib.auth.models import User
from luminary.models import Questions, Assignments, QuestionTags, OperationalDefinitions
from datetime import *

#Matt practicing with forms.
class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)


class OperationalDefinitions_Form1(forms.ModelForm):
    class Meta:
        model = OperationalDefinitions
        fields = ['question1']

#Other Forms
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password', 'first_name', 'last_name', 'is_superuser']
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Email Address',
                                            'autofocus': 'autofocus',
                                            'type': 'email'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}, ),
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}, ),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
            'is_superuser': forms.CheckboxInput(),
        }


class AdminForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'is_staff']
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Email Address',
                                            'autofocus': 'autofocus',
                                            'type': 'email'}),
            'is_staff': forms.CheckboxInput(),
        }


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Questions
        fields = ['question', 'type', 'image', ]
        widgets = {
            'question': forms.TextInput(attrs={'class': 'form-control ng-scope ng-binding editable',
                                              'placeholder': 'Question',
                                              'autofocus': 'autofocus'}),
            'type': forms.Select(attrs={'class': "form-control"}),
            'image': forms.FileInput(attrs={'class': 'form-control'}),
        }


class QuestionTagsForm(forms.ModelForm):
    class Meta:
        model = QuestionTags
        fields = ['tag']
        widgets = {
            'tag': forms.TextInput(attrs={'class': 'form-control',
                                          'placeholder': 'Tag',
                                          'data-role': 'tagsinput'}),
        }


class AssignmentForm(forms.ModelForm):
    class Meta:
        model = Assignments
        fields = ['point_value', 'title', 'due_date', 'type']
        widgets = {
            'due_date': forms.DateTimeInput(attrs={'class': 'form-control',
                                                   'placeholder': 'Due Date',
                                                   'autofocus': 'autofocus',
                                                   'type': 'datetime',
                                                   'name': 'due_date'}),
            'point_value': forms.NumberInput(attrs={'class': 'form-control',
                                                    'placeholder': 'Point Value',
                                                    'type': 'number',
                                                    'min': '0',
                                                    'name': 'point_value'}),
            'title': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Assignment Name',
                                            'name': 'title'}),
            'type': forms.Select(),
        }


QuestionTagsFormSet = inlineformset_factory(Questions, QuestionTags, can_delete=False, max_num=1, form=QuestionTagsForm)
