'''
Created on Sep 15, 2013

@author: steven
'''
from django.conf.urls import patterns, url
from views import link_index, register, create_user, authorize, logout_view


urlpatterns = patterns ('',
    url('^$', link_index, name='index'),
    url(r'^(?i)register/', register),
    url(r'^(?i)createuser/', create_user),
    url(r'^(?i)authenticate/', authorize),
    url(r'^(?i)logout/', logout_view, name='logout'),
)