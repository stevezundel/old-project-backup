#Login Views
from forms import UserForm
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseForbidden
from django import template
from django.template.context import Context
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from luminary.views import home
from django.core.exceptions import PermissionDenied
title = 'Lumi n Ary'


def link_index(request):
    form = UserForm
    dictionary = {'title': 'Lumi n Ary', 'form': form}
    return render(request, 'login/templates/login.html', dictionary)


def authorize(request):
    username = request.POST['email']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            request.session['email'] = username
            if user.is_staff:
                request.session['is_staff'] = True
    return redirect(home)


def register_test(request):
    t = template.loader.get_template("login/templates/register.html")
    c = Context({"title": "Lumi-n-Aries"})
    return HttpResponse(t.render(c))


def create_user(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            User.objects.create_user(form.cleaned_data['email'],
                                     form.cleaned_data['email'],
                                     form.cleaned_data['password'],
                                     first_name=form.cleaned_data['first_name'],
                                     last_name=form.cleaned_data['last_name'])
            return redirect(link_index)
    else:
        form = UserForm() 
    return render(request, 'login/templates/register.html', {'form': form})


def register(request):
    return redirect(home)


def logout_view(request):
    logout(request)
    return redirect(home)
