from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password', 'first_name', 'last_name', 'is_superuser']
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Email Address',
                                            'autofocus': 'autofocus',
                                            'type': 'email'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}, ),
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'}, ),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'}),
            'is_superuser': forms.CheckboxInput(),
        }


class AdminForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'is_staff']
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control',
                                            'placeholder': 'Email Address',
                                            'autofocus': 'autofocus',
                                            'type': 'email'}),
            'is_staff': forms.CheckboxInput(),
        }