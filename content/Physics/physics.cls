\newcommand{\maketableofcontents}{
  \chapterheadskip .125cm
  \chapter*{\contentsname}
%\pagestyle{myheadings} 
%TABLE OF CONTENTS -- CONTINUED
%\markboth{TABLE OF CONTENTS -- CONTINUED}{}
  \vspace{7.5\p@}
  \begingroup 
    \sloppy
    \normalsize
    \@starttoc{toc}
  \endgroup
  }