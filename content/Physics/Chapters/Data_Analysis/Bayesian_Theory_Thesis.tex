\chapter{ANALYSIS TECHNIQUES}\label{chap:analysis}
%%Insert
The goal of our analysis is to take the models described in previous chapters
and evaluate their goodness given some data.
Bayesian inference is a powerful tool for comparing models and finding
probability distributions for model parameters.  Although frequentist
statistics are also widely used, Bayesian data analysis is becoming more
prevalent.  In frequentist statistics, an experiment is repeated multiple times
and a resulting frequency distribution of outcomes for some event is
constructed.  Model parameters are considered fixed, meaning we focus on the
probability of the data given those parameters.  Bayesian inference reflects
our belief in some model, rather than a frequency distribution for some event.
The data is considered fixed, meaning we focus on the probability of some
hypothesis or set of model parameters given the data, and the model parameters
are allowed to vary to find a best match between our model and the data.

Bayesian inference is particularly useful in gravitational wave astronomy (and
astronomy in general). We are not able to repeat our experiments.  We must take
what the Universe gives us.  We do not, as of this writing, have a single
direct detection of gravitational waves.  As the field matures and we make the
first and then a handful of detections, we seek to make inferences from the
data with our limited information.  This is the strength of Bayesian analysis.
In this chapter, we develop an end-to-end Bayesian analysis pipeline that is
able to search for, characterize and assign confidence levels for the detection
of a stochastic gravitational wave background.


\section{Bayesian Basics}

Bayes' theorem can be derived from the product rule of conditional probability. 
In conditional probability, for two events A and B, we may write:

\begin{equation}
p(A|B)
\end{equation}
which we would read as the probability of A given that B is true.  Anything to
the right of the vertical bar is given as true.  If we add in event C, we can
write the conditional probability of both A and B given that C is true as
\begin{equation}
p(A,B|C),
\end{equation}
where the comma denotes ``and''.  The product rule of conditional probability
is
\begin{equation}
p(A,B) = p(B)p(A|B),
\end{equation}
which reads as the probability of A and B is the probability of B times the
probability of A given that B is true.  The product is commutative and we could
just as easily have written
\begin{equation}
p(A,B) = p(A)p(B|A).
\end{equation}
If we add in event C as before, we can write
\begin{equation}\label{eqn:product_rule}
p(A,B|C) = p(A|C)p(B|A,C) = p(B|C)p(A|B,C).
\end{equation}
Bayes' Theorem is a simple rearrangemenet of the product rule
(Eq.~\ref{eqn:product_rule}).
\begin{equation}
p(B|A,C) = \frac{p(B|C)p(A|B,C)}{p(A|C)}
\end{equation}

Our goal in science is to make inferences about some hypothesis based on
information we already have and any new data we measure or observe.  Let $C$ be
our existing information, $B$ be some hypothesis of interest, and $A$ be our
measured or observed data.
Following the treatment in \cite{gregory_bayesian_2010}, we can then write
Bayes' Theorem as
\begin{equation}
p(H|d,I) = \frac{p(H|I)p(d|H,I)}{p(d|I)}
\end{equation}
In words, the term on the left is the probability of our hypothesis $H$ given
our existing information and our new data.  It is called the a posteriori
distribution or the posterior probability distribution.  The first term on the
right, $P(H|I)$ is the a priori or prior probability distribution of
$H$.  It is the probability of hypothesis $H$ based on all the information we
have available prior to collecting our new data.  The term $P(d|H,I)$ is called the
likelihood.  It is the probability, or likelihood, that we would have obtained
data $d$ given that our hypothesis $H$ and prior information $I$ are true.  The
term in the denominator, $P(d, I)$, is sometimes called the marginal likelihood.  It
serves as a normalizing factor to ensure that the posterior distribution is a
proper probability distribution.  It is the sum of all possible outcomes of the
numerator.

If we consider a case with only two valid hypotheses, the marginal likelihood is
given by the law of total probability
\begin{equation}
p(d|I) = p(d|H_1, I)p(H_1) + p(d|H_2, I)p(H_2|I),
\end{equation}
which for a discrete number of hypotheses generalizes to
\begin{equation}
p(d|I) = \sum_{i=1}^N p(d|H_i,I)p(H_i,I).
\end{equation}

Often we will only be
interested in comparing two hypotheses.  In that case, we can avoid calculating
the marginal likelihood since Bayes' theorem for two different hypotheses and
the same data will have the same marginal likelihood.  Instead we look at what
is called an odds ratio between the two models.  The odds ratio gives the odds
that one model is preferred above another.  We take the ratio of the posterior
distributions and the marginal likelihood cancels out giving:
\begin{equation}\label{eqn_Bayes}
\frac{p(H_2|d,I)}{p(H_1|d,I)} =
\frac{p(d|H_2,I)}{p(d|H_1,I)}\frac{p(H_2|I)}{p(H_1|I)}
\end{equation}
The term involving the likelihoods is known as the Bayes' factor.  As discussed
in \cite{kass1995bayes}, the equation can be expressed in words as:
\begin{equation}
\text{posterior odds} = \text{Bayes Factor} \times \text{prior odds}
\end{equation}
The Bayes' factor transforms the prior odds ratio to the
posterior odds ratio.
For uniform priors, the Bayes' factor is equal to the posterior odds ratio.

\vspace{3ex}
\subsection{Gravitational Wave Applications}

In gravitational wave astronomy, the hypotheses we test are various models that
describe the data.  Typically, a model will be a waveform that describes how the
gravitational radiation from some system depends on the parameters of that
system.  For example, using general relativity we can derive
the expected gravitational radiation from a white dwarf binary
system~\cite{Peters:1963ux}, and we get the equations shown
earlier, Eqs.~\ref{WD_waveform}.

\subsubsection{Parameter Estimation and Model Selection}

\hspace{0.1in} Given some waveform, we can use Bayes' theorem to test which
waveform parameters give the best fit to our data.  We denote all
the parameters of a system with a parameter vector, $\vec{\lambda}$.  We
rewrite Bayes' theorem using d for our data again, and our hypothesis is some
model for the gravitational waveform, which we denote as $M$.  We have dropped
all the $|I)$ terms and implicitly assume them for the remainder of the paper.

\begin{equation}
p(\vec{\lambda}|d, M) = \frac{p(d|\vec{\lambda}, M)p(\vec{\lambda}|M)}{p(d|M)}
\end{equation}

The posterior distribution gives what is called the posterior distribution
function (PDF) for each parameter.  It is a distribution of the expected values
for a given parameter.  The highest mode of a PDF is called the maximum a
posteriori (MAP) estimate for that parameter~\cite{Cornish:2007if}, and the
width of the PDF gives the uncertainty for the parameter.
The distributions reflect our degree of belief in the set of parameters.

Generally, the model parameters can take on any values over some range.  We
obtain the marginal likelihood by integrating over all possible values of
$\vec{\lambda}$,
\begin{equation}
p(d| M) = \int{d\vec{\lambda}p(d|\vec{\lambda}, M)p(\vec{\lambda}| M)}.
\end{equation}
Notice that the marginalization over $\vec{\lambda}$ gives us the likelihood
function for model $M$, $p(d|M)$.  We then use Eq.~\ref{eqn_Bayes} to
compare two models and evaluate which is better supported by the data.  We form the
posterior odds ratio which we denote as $O$.
\begin{equation}
O \equiv \frac{p(M_1|d)}{p(M_0|d)} = \frac{p(d|M_1)p(M_1)}{p(d|M_0)p(M_0)}
\end{equation}
where we have used the indices 0 and 1 to denote the two different models
$M_1$ and $M_0$.  The two different models may be a waveform with different
numbers of parameters.  For example, consider a white dwarf binary system.
Most binary systems will not have a rapidly evolving frequency.  The data for
some binary may be better fit by a 7-dimensional model that excludes the
frequency derivative than by the 8-dimensional model~\cite{Littenberg:2009bm}. Other
examples include comparing the waveform for a black hole system given by general
relativity to the waveform from an alternative theory of
gravity~\cite{Cornish:2011ys}.
Later we will compare various models
for stochastic gravitational wave data.  One model contains instrument noise and
a stochastic gravitational wave background, another is instrument noise
only, and a third contains the galactic confusion foreground signal.  Comparing
two models using a Bayes' factor is called model selection.
If model 1 has a higher posterior value, the ratio above will be greater than
one.  A ratio of 1 means neither model is preferred over the other.  To make a
confident detection we say that we need a Bayes' factor of 30 or greater.
Given the example above of a model with noise only versus a model with noise and
a stochastic background, we are asking whether there is enough information in
the data to state that our signal model provides a better fit to the data than
does the noise model alone.






\begin{comment}
However, 


For a gravitational wave example of Bayes' theorem, let's consider the problem
of detecting gravitational waves from a white dwarf binary.  The gravitational
radiation from the system depends on a number of properties of the binary.  All
of the parameters that affect the gravitational radiation are represented by a
parameter vector, $\vec{\lambda}$.  We may already know something about the
properties of white dwarf binaries from electromagnetic observations or
theoretical considerations.  For example, we don't expect to find any white
dwarf stars whose mass is above the Chandresekhar limit.  We can quantify our
belief in the possible values of a given parameter as some distribution.
This is the prior in Bayes' theorem, p($\vec{\lambda}$).
The likelihood is the probability that we would have measured the data, $d$
given some set of model parameters, p($\vec{\lambda}$). We write the likelihood as
$p(d|\vec{\lambda}, M)$.  The evidence $p(d, M)$, is a normalizing factor
describing all the possible outcomes for the data $d$.  It is given by integrating over the
numerator in Bayes' theorem.


In terms of our parameter vector $\vec{\lambda}$, and some model $M$ describing how
the gravitational radiation depends on the parameters, we can rewrite Bayes'
theorem as
\begin{equation}\label{bayestheorem}
p(\vec{\lambda}|d, M) = \frac{p(d|\vec{\lambda}, M)p(\vec{\lambda},M)}{p(d,M)}
\end{equation}
where $p(\vec{\lambda}|d)$ is the computed posterior.
This represents our updated understanding of the probability of the parameters
$\vec{\lambda}$ representing the white dwarf binary after we have taken the
data into account.  The distribution gives our degree of uncertainty, estimation
of the paramter.  

The marginal likelihood is given by integrating over

We can then use the likelihoods to calculate a Bayes factor to compare two
models.















We form what is called an Odds ratio by taking the ratio of
the PDFs.


The ratio of the priors is know as the prior odds, and the ratio of the model
evidences calculated above is known as the Bayes factor.  The ratio of the
posterior distributions is the posterior odds.  Expression the equation in words
gives:


In order for our new information to change our belief, the Bayes factor must
overwhelm the prior odds.  In the case of uniform priors, the Bayes factor is
equal to the posterior odds ratio.  

This is simply 
the probability of one model given the data, versus another model given the
same data~\cite{Cornish:2007if}.



\subsection{Parameter Estimation}

  

\subsection{Model Selection}
Model selection is an important tool in gravitational wave astronomy.  For
example, it can be used to ask whether the data from a black hole inspiral
better supports General relativity or an alternative theory of gravity. In this
work, we study whether the data better supports a model with instrument
noise and a galactic foreground only, or a model that may a stochastic
background as well.

After marginalizing over the parameters, $\vec{\lambda}$, Bayes' theorem
becomes:

\begin{equation}\label{bayestheorem}
p(M|d) = \frac{p(d|M)p(M)}{p(d)}.
\end{equation}
where $p(M|d)$ can be interpreted as the probability that $M$ is the appropriate
model for the data.  The evidence, $p(d|M)$ is defined above in Eqn.~\ref{}


In principle, we could ask whether a model with instrument noise only is the best
of all models.  However, this would require normalizing over all possible
models which, even if all models could be know, would not be computationally feasible.

Instead, we can compare two models such that the common normalization factor
cancels out.
\end{comment}


\section{MCMC Techniques}
%Metropolis-Hastings
%MCMC basics
%Parallel Tempering
%Thermodynamic Integration
In principle, all of our work is done at this point.  We turn the Bayes' theorem
crank and it returns a posterior distribution.   In practice, there are often
difficulties in applying Bayes' theorem to real world problems.  As mentioned
above, often the marginal likelihood cannot be calculated analytically and can
be computationally expensive to calculate numerically.  Instead, we
use Markhov chain Monte Carlo (MCMC) techniques to sample the posterior
distribution.

\vspace{0.3in}
\subsection{Metropolis Hastings}
The term MCMC can refer to a
large range of stochastic data analaysis techniques.  In our work we use a
Metropolis Hastings MCMC algorithm to explore the parameter space of our model.
We keep a sequential history of locations visited in parameter space, which is
referred to as a chain.
A chain is started at some particular location in parameter space, and the
algorithm then proposes to move to a different location.
We evaluate the un-normalized posterior (likelihood $\times$ prior density) at
the new location and compare it to the un-normalized posterior at the old
location using a Hastings ratio.
\begin{equation}
H_{x\rightarrow y} =
\frac{L(\vec{y})p(\vec{y})q(\vec{x}|\vec{y})}{L(\vec{x})p(\vec{x})q(\vec{y}|\vec{x})}
\end{equation}
where $p$ and $q$ are the priors and distributions from which we draw to  make
our jumps to go from position x to position y respectively.  Note that the
Hastings ratio is very similar to the posterior odds ratio.  The only difference
is the inclusion of the jump distributions to maintain what is called detailed
balance.
By forming the ratio we are again able to avoid calculating the  marginal
likelihood. In the case of uniform priors and if we draw from the
same distribution, a gaussian, in both directions, our Hastings ratio is simply
the ratio of the likelihoods,
\begin{equation}
H_{x\rightarrow y} = \frac{L(\vec{y})}{L(\vec{x}))}.
\end{equation}

We then compare the Hastings ratio to a randomly drawn number between 0 and 1. 
If the new location has a higher likelihood, the Hastings ratio will be greater
than one, and hence greater than the random number drawn, and the jump will be accepted.  If
the new position is worse, the jump may or may not be accepted depending on
whether or not the Hastings ratio is greater than or less than the random number. 
Allowing jumps to poorer likelihood values gives the algorithm a chance to
explore the whole parameter space and not get stuck on a local maximum.  The
time the chain spends at each location in parameter space is the posterior
weight for that location.  By histogramming the MCMC chain after it is done
running, we map out the PDF for each parameter.

\subsection{Parallel Tempering}

Given enough time, our algorithm will find the MAP values for the model
parameters.
However, the MCMC algorithm only guarantees convergence after an infinite time.  To expedite the
process we use a Parallel Tempered Markhov chain Monte Carlo
(PTMCMC)~\cite{swendsen1986replica} search.
PTMCMC searches the data with multiple chains, each at a different ``heat''. The
unnormalized posterior becomes

\begin{equation}
p(\vec{\lambda}|d) = p(d|\vec{\lambda})^{\beta}p(\vec{\lambda})
\end{equation}
where $\beta$ is the heat for some chain.  The heats are calculated by
\begin{equation}\label{eqn:thermInt_beta}
\beta_i = \frac{1}{(T_{\text{max}})^{i/\text{NC}}}
\end{equation}
where $i$ labels the chains and $\text{NC}$ is the total number of chains.  
The effect of the heat is to ``melt'' or smooth the likelihood surface.  The
peaks in the likelihood surface become smooth and the hotter chains don't stick on
local maxima. Chains are allowed to propose parameter swaps, such that if a
cold chain is stuck on some local maximum, it has a chance to get moved off that
local maximum by exchanging parameters with a hotter chain.
The maximum
temperature, $T_{\text{max}}$, is chosen such that the surface will be
smooth enough for the hottest chain to freely explore the entire prior volume.

\vspace{1ex}
\subsection{Thermodynamic Integration}
The Metropolis Hastings algorithm allows us to do parameter estimation without
having to calculate the marginal likelihood.  However, as mentioned earlier, the
likelihood for a model is given by marginalizing over all model parameters
$\vec{\lambda}$.  If we want to do model selection, we need a way to calculate
the marginal likelihood of the waveforms.  Luckily, not only does a PTMCMC
algorithm allow us to explore the parameter space more efficiently, it also
enables us to calculate the marginal likelihood so that we can do model
selection.
This technique is known as thermodynamic
integration~\cite{gelman1998simulating, lartillot2006computing,
gregory_bayesian_2010, goggans2004using}, which parallels techniques used in
thermodynamics as the name suggests.

Thermodynamic integration uses the chains of different heats used in parallel
tempering to calculate the marginal likelihood, which we need for the
Bayes' factor calculation between models.  To help clean up the algebra in
what follows, we generalize the method with an unnormalized
probability distribution $q(\vec{\lambda})$, and a true probability distribution $p(\vec{\lambda})$ given by the normalization factor
$Z$.
This corresponds to making the following substitions:
\begin{eqnarray}\label{eqn_thermo_defs}
q(\vec{\lambda}) &=& p(d | \vec{\lambda}, M) p(\vec{\lambda} | M) \nonumber \\
q(\vec{\lambda}, \beta) &=& p(d | \vec{\lambda}, M)^{\beta} p(\vec{\lambda} | M)
\nonumber
\\
p(\vec{\lambda}) &=& p(\vec{\lambda} | d, M) \nonumber \\
Z &=& p(d | M) = \int{p(d|\vec{\lambda}, M)p(\vec{\lambda} | M)d\vec{\lambda}}.
\end{eqnarray}
Near the end of the derivation we will substitute back in the more specific
distributions to make the application to gravitational wave astronomy more
clear.  

It may not be possible to calculate Z directly, but we can calculate it
by defining a partition function:
\begin{equation}
Z(\beta) \equiv \int{q({\vec{\lambda}}, \beta) d\vec{\lambda}}
\end{equation}
where $\beta$ ranges from 0 to 1.
We then take the natural log of $Z$ and differentiate with respect to $\beta$.

\begin{equation}
\frac{\partial{\ln Z(\beta)}}{\partial \beta} =
\frac{1}{Z}\frac{\partial}{\partial \beta}Z = \frac{1}{Z}\frac{\partial}{\partial \beta} \int{
q(\vec{\lambda},\beta)d\vec{\lambda}}
\end{equation}
Assuming the interchangebility of integration and differentiation, and following
the steps in \cite{lartillot2006computing}, we get:
\begin{eqnarray}
\frac{\partial{\ln Z(\beta)}}{\partial \beta} &=&  
\frac{1}{Z}\frac{\partial}{\partial \beta} \int{q(\vec{\lambda},
\beta)d\vec{\lambda} } \nonumber \\
&=&  \int{d\vec{\lambda} \frac{\partial}{\partial \beta} q(\vec{\lambda}, \beta)}
\frac{1}{Z} \nonumber \\
&=& \int{d\vec{\lambda} \frac{1}{q(\vec{\lambda}, \beta)}{\frac{\partial}{\partial \beta}
q(\vec{\lambda}, \beta)} \frac{q(\vec{\lambda}, \beta)}{Z}} \nonumber \\
&=& \int{d\vec{\lambda} \frac{\partial \ln q(\vec{\lambda},
\beta)}{\partial \beta} \frac{q(\vec{\lambda}, \beta)}{Z}} \nonumber \\
&=& \int{d\vec{\lambda} \frac{\partial \ln q(\vec{\lambda},
\beta)}{\partial \beta} p(\vec{\lambda}, \beta)}
\end{eqnarray}
where $p(\vec{\lambda}, \beta)$ is the properly normalized posterior
distribution. The last line is simply the expectation value of the log term.

\begin{equation}\label{eqn:expectation}
\frac{\partial{\ln Z(\beta)}}{\partial \beta} =
E\left[\frac{\partial \ln q(\vec{\lambda},
\beta)}{\partial \beta}\right]
\end{equation}

From parallel tempering, we already have a good parameterizing factor $\beta$
which ranges from 0 to 1.  
Using Eqs.~\ref{eqn_thermo_defs}, Eq.~\ref{eqn:expectation} becomes

\begin{eqnarray}
\frac{\partial{\ln Z(\beta)}}{\partial \beta} &=&
\left < \frac{\partial \ln
\left(p(d|\vec{\lambda},M)^{\beta}p(\vec{\lambda},M)\right)}{\partial \beta} \right >
\nonumber \\
&=& \left< \frac{\partial}{\partial\beta}\left(\ln p(d|\vec{\lambda},M)^{\beta}
+ \ln p(\vec{\lambda},M)\right)\right> \nonumber \\
&=& \left< \frac{\partial}{\partial\beta}\left(\beta\ln
p(d|\vec{\lambda},M)\right)\right> \nonumber \\
&=& \left< \ln p(d|\vec{\lambda},M) \right>.
\end{eqnarray}
Now we can integrate over $\beta$,
\begin{equation}
\ln Z_1 - \ln Z_0 = \int_0^1{d\beta\langle \ln
p(d|\vec{\lambda},M)\rangle_{\beta}}.
\end{equation}
But from Eq.~\ref{eqn_thermo_defs}, we have

\begin{equation}
Z_{\beta} = p_{\beta}(d|M)=\int(p(d|\vec{\lambda},M)^{\beta}p(\vec{\lambda}|M)).
\end{equation}
For $\beta=1$, this gives $Z=p(d|M)$, and for $\beta=0$, $\ln Z_0=0$.  We are
left with

\begin{equation}\label{eqn_logLike}
\ln p(d|M) = \int_0^1\left< \ln p(d|\vec{\lambda},M) \right>d\beta
\end{equation}
which is what we were looking for.  The Bayes' Factor is given by:

\begin{equation}
\ln B_{10}=\frac{\ln p(d|M_1)}{\ln p(d|M_0)}
\end{equation}

The integral in Eq.~\ref{eqn_logLike} can be
approximated using the chains from our parallel tempering.  We can approximate
the expectation value by taking the average of all likelihood values visited by
each chain, and we can then perform the integral numerically.

The above formalism gives an end-to-end algorithm for finding posterior
distributions for all model parameters and for performing model selection
between pairs of models~\cite{Littenberg:2009bm}.  In the rest of the chapter,
we develop a Hierarchical Bayesian algorithm that also folds in the task of
modeling a source population distribution.

\section{Hierarchical Bayes}
In the case of our population study, we can do parameter estimation and model
selection simulataneously using a technique known as Hierarchical Bayes.
We develop here a simple yet comprehensive Hierarchical Bayesian modeling
approach that uses the full multi-dimensional and highly correlated parameter
uncertainties of a collection of signals to constrain the joint parameter
distributions of the underlying astrophysical models.  The method is general
and can be applied to any number of astrophysical model selection
problems~\cite{Mandel:2009xr, Loredo:2012jm, Soiaporn:2012md}.

A remarkable feature of the Hierarchical Bayesian method is that in its purest
form it is completely free of selection effects such as Malmquist bias. By
``purest form'' we mean where the signal model extends over the entire source
population, including those with vanishingly small signal-to-noise
ratio~\cite{Messenger:2012jy}. In practice it is unclear how to include
arbitrarily weak sources in the analysis, and in any case the computational
cost would be prohibitive, so we are forced to make some kind of selection cuts
on the signals, and this will introduce a bias if left
uncorrected~\cite{Schutz:2011tw}.

To illustrate the Hierarchical Bayesian approach and to investigate where bias
can arise, we look at the problem of determining the population model for white
dwarf binaries in the Milky Way.  Future space-based missions are expected to
detect thousands to tens of thousands of white dwarf
binaries~\cite{AmaroSeoane:2012je, Crowder:2006eu, Nissanke:2012eh,
Timpano:2005gm, Littenberg:2011zg}. Here we focus on determining the spatial
distribution and the chirp mass distribution, but in future work we plan to
extend our study to include a wider class of population characteristics such as
those described in Ref.~\cite{Nissanke:2012eh}.
Determining the galaxy shape using gravitational wave observations of white
dwarf binaries will be an independent measure on the shape of the galaxy to
complement electromagnetic observations.  Additionally, the white dwarf
binaries that are not detectable form a very bright stochastic foreground. 
Accurately modeling the confusion foreground level is crucial for the detection
of extragalactic stochastic gravitational wave
signals~\cite{Adams:2010vc}.

Hierarchical Bayesian modeling has been around since at least the
1950's~\cite{Good:1965,1972_LindleySmith,Morris:1992,MacKay94},
but it is only now becoming widely known and used.  The term ``hierarchical"
arises because the analysis has two levels. At the highest level are the space
of models being considered, and at the lower level are the parameters of the
models themselves. Hierarchical Bayes provides a method to simultaneously
perform model selection and parameter estimation. In this work we will consider
models of fixed dimension that can be parameterized by smooth functions of one
or more hyperparameters. The joint posterior distribution for the model
parameters $\vec{\lambda}$ and the hyperparameters $\vec{\alpha}$ given data
$d$ follows from Bayes' theorem:
\begin{equation}\label{formal}
p(\vec{\lambda}, \vec{\alpha} \vert d) = \frac{ p(d \vert \vec{\lambda},
\vec{\alpha}) p(\vec{\lambda} \vert \vec{\alpha}) p(\vec{\alpha})}{ p(d)} \, ,
\end{equation}
where $p(d \vert \vec{\lambda}, \vec{\alpha})$ is the likelihood,
$p(\vec{\lambda} \vert \vec{\alpha})$ is the prior on the model parameters for a model described by hyperparameters $\vec{\alpha}$,
$p(\vec{\alpha})$ is the hyperprior and $p(d)$ is a normalizing factor
\begin{equation}
p(d) = \int p(d, \vec{\alpha}) d\vec{\alpha}= \int p(d \vert \vec{\lambda}, \vec{\alpha}) p(\vec{\lambda}
 \vert \vec{\alpha}) p(\vec{\alpha})  d\vec{\lambda} d\vec{\alpha} \, .
\end{equation}
The quantity $p(d, \vec{\alpha})$ can be interpreted as the ``density of
evidence'' for a model with hyperparameters $\vec{\alpha}$.

As was the case when calculating the marginal likelihood, the integral
marginalizing over the hyperparameters is often only tractable numerically,
and this can be computationally expensive. Empirical Bayes is a collection of
methods that seek to estimate the hyperparameters in various ways from the
data~\cite{Casella:1985, Carlin:2000}. The MCMC
techniques developed above allow us to implement Hierarchical Bayesian modeling
without approximation by producing samples from the joint posterior distributions,
which simultaneously informs us about the model parameters $\vec{\lambda}$ and
the hyperparameters $\vec{\alpha}$. This approach helps reduce systematic
errors due to mis-modeling, as the data helps select the appropriate model. An
example of this is the use of hyperparameters in the instrument noise model,
such that the noise spectral density is treated as an unknown to be determined
from the data~\cite{Cornish:2007if, Littenberg:2010gf, Adams:2010vc}.
Hierarchical Bayesian modeling can be extended to discrete and even disjoint
model spaces using the Reverse Jump Markov chain Monte Carlo
(RJMCMC)~\cite{green_highly_2003} algorithm. Each discrete model can be
assigned its own set of continuous hyperparameters.



\vspace{0.3in}
\subsection{Toy Model I}\label{toy1}

As a simple illustration of Hierarchical Bayesian modeling, consider some
population of $N$ signals, each described by a single parameter $x_i$ that is
drawn from a normal distribution with standard deviation $\alpha_0$. The
measured values of these parameters are affected by instrument noise that is
drawn from a normal distribution with standard deviation $\beta$. The maximum
likelihood value for the parameters is then $\bar{x}_i = \alpha_0 \delta_1 +
\beta \delta_2$ where the $\delta$'s are i.i.d. unit standard deviates. Now
suppose that we employ a population model where the parameters are distributed
according to a normal distribution with standard deviation $\alpha$. Each
choice of $\alpha$ corresponds to a particular model with posterior
distribution
\begin{equation}\label{eq:toy1post}
p(x_i\vert s, \alpha) = \frac{1}{p(s,\alpha)} \prod_{i=1}^N \frac{1}{(2\pi \alpha\beta)} 
e^{-(\bar{ x}_i - x_i)^2/2\beta^2} e^{-x_i^2/2\alpha^2} \, ,
\end{equation}
and model evidence
\begin{equation}\label{mla}
p(s,\alpha) = \frac{1}{(\sqrt{2\pi}\sqrt{\alpha^2+\beta^2})^N}\prod_i e^{-{\bar x}_i^2 /2 (\alpha^2+\beta^2)} \, .
\end{equation}
To arrive at a Hierarchical Bayesian model we elevate $\alpha$ to a
hyperparameter and introduce a hyperprior $p(\alpha)$ which yields the joint
posterior distribution
\begin{equation}\label{eq:toy1post2}
p(x_i, \alpha \vert s) = \frac{p(x_i\vert s, \alpha) p(\alpha)}{p(s)} \, .
\end{equation}
Rather than selecting a single ``best fit'' model, Hierarchical Bayesian
methods reveal the range of models that are consistent with the data. In the
more familiar, non-hierarchical approach we would maximize the model evidence
(Eq.~\ref{mla}) to find the model that best describes the data, which is here
given by
\begin{equation}
\alpha_{\rm ME}^2 = \frac{1}{N}\sum_{i=1}^N {\bar x}_i^2 - \beta^2 .
\end{equation}
Since ${\rm Var}({\bar x}_i)=\alpha_0^2+\beta^2$, we have
\begin{equation}\label{estx}
\alpha_{\rm ME}^2 = \alpha_0^2 \pm {\cal O}(\sqrt{2}(\alpha_0^2+\beta^2)/\sqrt{N}) \, .
\end{equation}
The error estimate comes from the sample variance of the variance estimate. In
the limit that the experimental errors $\beta$ are small compared to the width
of the prior $\alpha_0$, the error in $\alpha$ scales uniformly as
$1/\sqrt{N}$. The scaling is more complicated when we have a collection of
observations with a range of measurement errors. Suppose that the measurement
errors are large compared to the width of the prior, and that we have $N_1$
observations with standard error $\beta_1$, $N_2$ observations with standard
error $\beta_2$, etc., then the error in the estimate for $\alpha$ is
\begin{equation}
\Delta \alpha^2 = \left(\sum_i \frac{N_i}{\beta_i^4}\right)^{-1/2} \, .
\end{equation}
Recalling that $1/\beta_i$ scales with the signal-to-noise ratio of the
observation, we see that a few high SNR observations constrain $\alpha$ far
more effectively than a large number of low SNR observations.

The above calculation shows that the maximum evidence criteria provides an
unbiased estimator for the model parameter $\alpha_0$, but only if the
measurement noise is consistently included in both the likelihood and the
simulation of the $\bar{x}_i$. Using the likelihood from
(Eq.~\ref{eq:toy1post}) but failing to include the noise in the simulations
leads to the biased estimate $\alpha^2_{\rm ME} = \alpha_0^2 - \beta^2$.
Conversely, including noise in the simulation and failing to account for it in
the likelihood leads to the biased estimate $\alpha^2_{\rm ME} = \alpha_0^2 +
\beta^2$.
These same conclusions apply to the Hierarchical Bayesian approach,
as we shall see shortly.


\subsubsection{Numerical Simulation}

\hspace{0.1in} The joint posterior distribution (Eq.~\ref{eq:toy1post2}) can be
explored using MCMC techniques. To do this we produced simulated data with $N= 1000$,
$\alpha_0 = 2$, and $\beta=0.4$ and adopted a flat hyperprior for $\alpha$. The
posterior distribution function for $\alpha$, marginalized over the $x_i$, is
shown in Fig.~\ref{fig:toy1_alpha}. The distribution includes the injected
value, and has a spread consistent with the error estimate of (Eq.~\ref{estx}).
The Maximum-a-Posteriori (MAP) estimate for $\alpha$ has been displaced from the
injected value of $\alpha_0 = 2$ by the simulated noise.
\begin{figure}[h!]
   \centering
   \includegraphics[width=6.0in,angle=0] {Chapters/Data_Analysis/Toy_Alpha.png} 
   \caption{The marginalized PDF for $\alpha$. The
   injected value is indicated by the vertical black line.}
   \label{fig:toy1_alpha}
\end{figure}

\vspace{-3.5ex}

To test that there is no bias in the recovery of the model hyperparameter
$\alpha$, we produced 30 different realizations of the data and computed the
average MAP value.  Fig.~\ref{fig:toy1_MAPs} shows the MAP value for each of
these realizations and the corresponding average. We see that as we average
over multiple realizations $\alpha$ does indeed converge to the injected value.
The blue line in Fig.~\ref{fig:toy1_MAPs} shows a biased recovery for $\alpha$
when noise is not included in the data.
We instead recover $\alpha = \sqrt{\alpha^2_0 - \beta^2} \approx 1.96$.

\begin{center}
\begin{figure}[h!]
   \centering
   \includegraphics[width=6.0in] {Chapters/Data_Analysis/Toy_ModeMAP.png} 
   \caption{MAP values for 30 different simulations of the toy model.  The red
   curve includes noise in the simulated signal and converges to $\alpha_0$ as
   expected.  The blue curves does not include noise in the simulation and
   converges to $\alpha_0^2 - \beta^2$.}
   \label{fig:toy1_MAPs}
\end{figure}
\end{center}

\vspace{-.6in}
\subsection{Toy Model II}\label{toy2}

The Hierarchical Bayesian approach produces un-biased estimates for the model
parameters if the signal and the noise (and hence the likelihood) are correctly
modeled. However, in some situations the cost of computing the likelihood can
be prohibitive, and it becomes desirable to use approximations to the
likelihood, such as the Fisher Information Matrix. For example, to investigate
how the design of a detector influences its ability to discriminate between
different astrophysical models, it is necessary to Monte Carlo the analysis
over many realizations of the source population for many different instrument
designs, which can be very costly using the full likelihood.

To explore these issues we introduce a new toy model that more closely
resembles the likelihood functions encountered in gravitational wave data
analysis. Consider a waveform $h_0$ that represents a single data point ({\it
e.g.} the amplitude of a wavelet or a Fourier component), which can be
parameterized in terms of the distance to the source $d_0$. The instrument
noise $n$ is assumed to be Gaussian with variance $\beta^2$. Here we will treat
the noise level $\beta$ as a hyperparameter to be determined from the
observations. Adopting a fiducial noise level $\beta_0$ allows us to define a
reference signal-to-noise ratio ${\rm SNR}^2_0 = h_0^2/\beta_0^2$.
The likelihood of observing data $s= h_0+n$ for a source at distance $d$ with
noise level $\beta$ is then
\begin{equation}\label{liketoy}
p(s\vert d, \beta)  = \frac{1}{\sqrt{2\pi}\beta}e^{-(s-h)^2/(2\beta^2)},
\end{equation}
where $h = (d_0/d) h_0$. The likelihood is normally distrubuted in the inverse
distance $1/d$, with a maximum that depends on the particular noise realization
$n$,
\begin{equation}
\frac{1}{d_{\rm ML}} = \frac{1+n/(\beta_0 {\rm SNR}_0)}{d_0} \, .
\end{equation}
Now suppose that the distances follow a one-sided normal distribution
$p(d \geq 0) = \frac{2}{\sqrt{2\pi}\beta}\exp(-d^2/2\alpha_0^2)$, and that we
adopt a corresponding model for the distance distribution with hyperparameter
$\alpha$ and a flat hyperprior.

We simulate the data with $N=1000$ sources with $\alpha_0=2$ and $\beta =
0.05$. The values of $\alpha_0$ and $\beta$ were chosen to give a fiducial
${\rm SNR}=5$ for $d = 2\alpha_0$. In the first of our simulations the value of
$\beta$ was assumed to be known and we computed the MAP estimates of $\alpha$
for 30 different simulated data sets. As shown in Fig.~\ref{fig:toy2_MAPs},
the average MAP estimate for $\alpha$ converges to the injected value.

\begin{figure}[h!]
   \centering
   \includegraphics[width=6.0in,angle=0]
   {Chapters/Data_Analysis/DToy_FishFull.png} \caption{MAP values for 30 different realizations of the toy model II. Using the full likelihood (red) the
MAP values converge to the injected value, but with the Fisher Matrix approximation to the likelihood (blue) there
is a bias.}
   \label{fig:toy2_MAPs}
\end{figure}

In contrast to the first toy model where only the combination $\alpha^2+\beta^2$ is
constrained by the data, in this more realistic toy model both the noise level $\beta$ and
the model hyperparameter $\alpha$ are separately constrained. 
Fig.~\ref{fig:BetaAlpha} shows the marginalized PDFs for both $\beta$ and $\alpha$. Tests using
multiple realizations of the data show that the MAP values of $\alpha$ and $\beta$
are un-biased estimators of the injected parameter values.

\begin{figure}[h!]
   \centering
   \includegraphics[width=6.0in,angle=0]
   {Chapters/Data_Analysis/DToy_NoiseFit.png} \caption{PDFs for the prior
   hyperparameter $\alpha$ and the noise level $\beta$ for toy model II.  Both are individually constrained in this model.  The injected values are shown by the black lines.}
   \label{fig:BetaAlpha}
\end{figure}


\subsubsection{Approximating the Likelihood}

\hspace{0.1in} For stationary and Gaussian instrument noise the log likelihood
for a signal described by parameters $\vec{\lambda}$ is given by
\begin{equation}\label{logL}
L(\vec{\lambda}) =  -\frac{1}{2}(s-h(\vec{\lambda}) \vert s-h(\vec{\lambda})),
\end{equation}
where $(a\vert b)$ denotes the standard noise-weighted inner product, and we have
supressed terms that depend on
the noise hyperparameters. We expand the waveform
$h(\vec{\lambda})$ about the injected source parameters $\vec{\lambda}_0$ and
get
 \begin{equation}
h(\vec{\lambda}) =  h(\vec{\lambda}_0) + \Delta \lambda^i \bar{h}_{,i} + \Delta \lambda^i \Delta \lambda^j \bar{h}_{,ij}
+ \mathcal{O}(\Delta \lambda^3)
\end{equation}
where $\Delta \vec{\lambda} = \vec{\lambda}-\vec{\lambda}_0$, and it is
understood that the derivatives are evaluated at $\vec{\lambda}_0$. Expanding
the log likelihood we find:
\begin{eqnarray}\label{lex}
L(\Delta \vec{\lambda}) =&-&\frac{1}{2} (n\vert n) +\Delta \lambda^i (n \vert h_{,i})\nonumber \\
&-&\frac{1}{2} \Delta \lambda^i \Delta \lambda^j (h_{,i}\vert h_{,j})+ {\cal O}(\Delta \lambda^3)\, .
\end{eqnarray}
The maximum likelihood solution is found from $\partial L/\partial\Delta
\lambda^i = 0$, which yields $\Delta \lambda_{\rm ML}^i = (n \vert h_{j})
\Gamma^{ij}$, where $\Gamma^{ij}$ is the inverse of the Fisher Information
Matrix $\Gamma_{ij}=(h_{,i}\vert h_{,j})$. Using this solution to eliminate $(n
\vert h_{,i})$ from (Eq.~\ref{lex}) yields the quadratic, Fisher Information
Matrix approximation to the likelihood:
\begin{equation}\label{fish}
L(\vec{\lambda}) = {\rm const.} -\frac{1}{2} (\lambda^i - \lambda^i_{\rm ML})(\lambda^j- \lambda^j_{\rm ML}) \Gamma_{ij} \; .
\end{equation}
This form of the likelihood can be used in simulations by drawing the $\Delta
\lambda_{\rm ML}^i$ from a multi-variate normal distribution with covariance
matrix $\Gamma^{ij}$.

In our toy model $\Gamma_{dd} = {\rm SNR}^2_0 \beta_0^2/(\beta^2 d_0^2)$, and
$L(d)=-{\rm SNR}_0^2 \beta_0^2 (d-d_{\rm ML})^2/(2 \beta^2 d_0^2)$. The
approximate likelihood follows a normal distribution in $d$ while the full
likelihood follows a normal distribution in $1/d$. For signals with large SNR
this makes little difference, but at low SNR the difference becomes significant
and results in a bias in the recovery of the model hyperparameters, as shown in
Fig.~\ref{fig:toy2_MAPs}. In this instance there is a simple remedy: using $u
= 1/d$ in place of $d$ in the quadratic approximation to the likelihood exactly
reproduces the full likelihood in this simple toy model. However, it is not
always so easy to correct the deficiencies of the quadratic Fisher Information
Matrix approximation to the likelihood.