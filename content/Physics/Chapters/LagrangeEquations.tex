\chapter{Lagrange's Equations}\label{LagrangeEquations}

\section{Practice Problems}
These are some simple introductory physics problems. Solve for the equations of
motion in each case and we will revisit these problems later.

\subsection{Example - Free particle}
The equations of motion for a free particle are very simple.  Given some force,
we simply have
\begin{equation}
\bvec{F} = \dot{\bvec{p}}
\end{equation}
which in cases of constant mass can be expressed as
\begin{equation}
\bvec{a} = \frac{\bvec{F}}{m}.
\end{equation}
Finding the equations of motion means finding the differential equation that
describes the motion of the object.  In many cases that will mean finding an
expression for the acceleration.  Note that in general there will be more than
one equation of motion.  For this case of the free particle, there are 3
equations, one each for x, y, and z motion.

\subsection{Problem 1 - Block sliding down ramp}
Find the equations of motion for a block of mass m sliding down a frictionless
incline.  The angle of incline above the horizontal is $\theta$.

\begin{comment}
\subsubsection{Answer}
Use Newton's 2nd Law.  Net force is component of gravity pointing down ramp.
\begin{equation}
F=mg\sin{\theta}
\end{equation}
We get $a=g\sin{\theta}$, which can be expressed as
\begin{equation}
\ddot{x}=g\sin{\theta}
\end{equation}
\end{comment}

\subsection{Problem 2 - Atwood's Machine}
Find the equations of motion for an Atwood's Machine consisting of two blocks of
mass $m_1$ and $m_2$ connected by a massless string hanging over a massless,
frictionless pulley.

\subsection{Problem 3 - Pendulum}
Find the equations of motion for a pendulum of mass $m$ attached to the ceiling
by a massless string of length $l$, which oscillates in a plane.

\section{Degrees of Freedom and Generalized Coordinates}
Suppose we have a container with gas particles.  To describe the motion of the
gas particles, we would need to specify a radius vector for each particle.  Each
radius vector would have $3$ componenents, so for $n$ gas particles we would
have to specify $3n$ quantities to describe the positions of all the particles. 
If all the positions are independent of one another, we say that the system has
$3n$ degrees of freedom.  If on the other hand, there are relations between some
of the particles, we will have fewer degrees of freedom.  If there are $m$
equations relating the particle positions to each other, then we would have
$3n-m$ degrees of freedom.  The $m$ equations are known as equations of
constraint.

\vspace{0.5in}

\noindent \textbf{Diatomic Molecule:} How many degrees of freedom does a
diatomic molecule have?

\vspace{0.5in}

\noindent \textbf{Problems}  What are the equations of constraint for the four
problems in section 1?  How many degrees of freedom does each system have?

\vspace{0.5in}

We can use any coordinate system we wish to describe the motion of the
pendulum, such as cartesian coordinates, spherical, etc.  We can also use what
are known as generalized coordinates, where the number of coordinates is
equal to the number of degrees of freedom.  For example, we could specify the
position of a pendulum by it's x, y, and z position.  However, this is
redundant to some degree, because the pendulum does not have 3 degrees of
freedom.  Similarly for the diatomic molecule, if we know the position of one
atom, we need fewer than three more pieces of information to specify the
location of the other atom.

We only need one generalized coordinate for each degree of freedom.  The
generalized coordinates are not necessarily unique for a system.  What
coordinates can we use for the diatomic molecule and they 4 problems from
section 1?

\section{D'alembert's Principle}
\begin{comment}
\begin{enumerate}
  \item What does Goldstein mean by assume in equilibrium
  \item Are the assumptions all necessary?
\end{enumerate}
\end{comment}
D'alembert's principle is a reformulation of Newton's laws for situations where
forces of contraint exist.  D'alembert sought to eliminate these forces from his
formulation.  He introduced the idea of a virtual displacement, which is an
infinitesimal displacement of a system that occurs at some particular time.  It
is called virtual to distinguish it from an actual infinitesimal displacement
that occurs during some time interval $dt$.  

D'alembert's principle states the the net virtual work done by the forces of
constraint is zero.  This is basically equivalent to saying the the forces of
constraint are perpendicular to the surface of constraint.  A simple example
that we have all seen before is the motion of a block on an incline.  The normal
force of the block on the incline cancels a component of gravity, constraining
the block to move on the incline.  We restrict ourselves to systems that satisfy
these conditions.  We can write:

\begin{equation}
\sum_i \bvec{F}_i \cdot \delta \bvec{r}_i = 0
\end{equation}
where the subscript i labels different particles in the system and
$\bvec{r}_i$ is the position vector for the ith particle.  We can now break up
the force $\bvec{F}$ into external applied forces, $\bvec{F}^{\text{ext}}$, and
forces of constraint, $\bvec{f}$.  We get:

\begin{equation}
\sum_i (\bvec{F}^{\text{ext}}_i + \bvec{f}_i) \cdot \delta \bvec{r}_i = 0
\end{equation}
which becomes
\begin{equation}
\sum_i \bvec{F}^{\text{ext}}_i \cdot \delta \bvec{r}_i + \sum_i \bvec{f}_i \cdot
\delta \bvec{r}_i = 0
\end{equation}
Since we are restricting ourselves to systems where the the forces of
constraint do no work, the second term is zero and we are
left with:

\begin{equation}
\sum_i \bvec{F}^{\text{ext}}_i \cdot \delta \bvec{r}_i = 0
\end{equation}
We have eliminated the forces of constraint, but in general,
$\bvec{F}^{\text{ext}}_i \neq 0$.

\vspace{0.5in}
\noindent \textbf{Question:}  The above equation could only be used for static
situations.  How could we change it for dynamic systems?
\vspace{0.5in}

Bernoulli first thought of the following idea and D'alembert developed it. 
Newton's 2nd Law gives:

\begin{equation}
\bvec{F}_i = \dot{\bvec{p}}_i
\end{equation}

We can rearrange the equation of motion and get:
\begin{equation}
\bvec{F}_i - \dot{\bvec{p}}_i = 0
\end{equation}
So instead of our original formulation of virtual work, we can write:
\begin{equation}
\sum_i (\bvec{F}_i-\dot{\bvec{p}}_i)\cdot \delta \bvec{r}_i = 0
\end{equation}
We can again separate out the forces of constraint.
\begin{equation}
\sum_i (\bvec{F}^{\text{ext}}_i-\dot{\bvec{p}}_i)\cdot \delta \bvec{r}_i + \sum_i \bvec{f}_i \cdot \delta
\bvec{r}_i = 0
\end{equation}
and we are left with:
\begin{equation}
\sum_i (\bvec{F}^{\text{ext}}_i-\dot{\bvec{p}}_i)\cdot \delta \bvec{r}_i = 0
\end{equation}

\vspace{0.5in}
\noindent \textbf{Problem:} Compare and contrast D'alembert's Principle to
conservation of Energy.

\section{Lagrange's Equations}
We successfully expressed the virtual work in a form that didn't include the
forces of constraint.  However, it could still be placed in a more convenient
form if the coordinates were independent of each other.

\vspace{0.5in}
\noindent \textbf{Why?}
\vspace{0.5in}

We want to transform to our generalized coordinate system.  Suppose we have
coordinates $\bvec{r}_i$ that are related to our generalized coordinates $q$.

\begin{equation}
\bvec{r}_i = \bvec{r}_i(q_1, q_2, \ldots, q_n, t)
\end{equation}
Using the rules of partial differentiation, give an expression for
\begin{equation}
\bvec{v}_i \equiv \frac{d\bvec{r}_i}{dt}
\end{equation}
in terms of the $q$s.
\subsubsection{Answer}
\begin{equation}
\bvec{v}_i \equiv \frac{d\bvec{r}_i}{dt} = \frac{\partial \bvec{r}_i}{\partial
q_j}\frac{dq_j}{dt} + \frac{\partial \bvec{r}_i}{\partial t}
\end{equation}

When working with virtual displacements, we use $\delta x$, $\delta y$, etc.,
instead of $dx$, $dy$, etc.  We do this to remind ourselves that the virtual
displacement happen at a specific time and not over some time interval $dt$.

\vspace{0.5in}
\noindent Express $\delta \bvec{r}_i$ in terms of the $q$s.

\subsubsection{Answer}
\begin{equation}
\delta \bvec{r}_i = \frac{\partial \bvec{r}_i}{\partial q_j}\delta q_j
\end{equation}

Using your expressions from above, we can re-express the virtual work.
\begin{equation}
\sum_i \bvec{F}_i \cdot \delta \bvec{r}_i = \sum_j Q_j \delta q_j
\end{equation}
Solve for $Q$ using your expressions from above.  What is the physical
interpretation of $Q$.

\subsubsection{Answer}
\begin{equation}
Q_j = \sum_i\bvec{F}_i\frac{\partial \bvec{r}_i}{\partial q_j}
\end{equation}

Next we want to re-express the term involving the momentum in D'alembert's
principle.  We have:
\begin{equation}
\sum_i \dot{\bvec{p}}_i \cdot \delta \bvec{r}_i = \sum_i m_i\ddot{\bvec{r}}_i
\cdot \delta \bvec{r}_i
\end{equation}
Manipulate this expression around until you get the following expression:
\begin{equation}
\frac{d}{dt}\left(\frac{\partial T}{\partial \dot{q}_j}\right) - \frac{\partial
T}{\partial q_j} = Q_j
\end{equation}
where $T=\frac{1}{2}m_iv_i^2$ is the kinetic energy.  This requires a fair bit
of work and mathematical ingenuity.  Don't worry if you get stuck.  Keep trying
and you can work through it together as a class.  Here are a couple of relations
that you will want to show and use along the way.
\begin{equation}
\frac{d}{dt}\left(\frac{\partial \bvec{r}_i}{\partial q_j}\right) =
\frac{\partial \bvec{v}_i}{\partial q_j}
\end{equation}
\begin{equation}
\frac{\partial{\bvec{v}_i}}{\partial \dot{q}_j} = \frac{\partial
\bvec{r}_i}{\partial q_j}
\end{equation}

\subsubsection{Answer}
Starting from
\begin{equation}
\sum_i m_i\ddot{\bvec{r}}_i
\cdot \delta \bvec{r}_i
\end{equation}
pull out a time derivative and substract of the second part of the chain rule
that comes from doing so.
\begin{equation}
\sum_i m_i\ddot{\bvec{r}}_i =
\sum_i\left[\frac{d}{dt}\left(m_i\dot{\bvec{r}}_i\cdot\frac{\partial
\bvec{r}_i}{\partial
q_j}\right)-m_i\dot{\bvec{r}}\cdot\frac{d}{dt}\left(\frac{\partial
\bvec{r}_i}{\partial q_j}\right)\right]
\end{equation}
In the last term, we interchange the differentiation with respect to $t$ and
$q_j$.
\begin{equation}
\frac{d}{dt}\left(\frac{\partial \bvec{r}_i}{\partial q_j}\right) =
\frac{\partial\dot{\bvec{r}}_i}{\partial q_j} = \sum_k\frac{\partial^2
\bvec{r}_i}{\partial q_j\partial
q_k}\dot{q}_k+\frac{\partial^2\bvec{r}_i}{\partial q_j \partial
t}=\frac{\partial \bvec{v}_i}{\partial q_j}
\end{equation}
From our expression for the velocity above we can get
\begin{equation}
\frac{\partial \bvec{v}_i}{\partial \dot{q}_j} =
\frac{\partial{\bvec{r}_i}}{\partial q_j}
\end{equation}
You should be able to hack through the rest of this with help from what Alex had
on the board in class.  Basically you plug in the velocity expressions into the
long expression above.

\vspace{1in}
Now assuming that the forces are derivable from a scalar potential V,
\begin{equation}
\bvec{F}_i = -\nabla_i V
\end{equation}
re-express your equation for $Q$ in terms of $V$.
Use this $Q$ to show that 
\begin{equation}
\frac{d}{dt}\left(\frac{\partial L}{\partial \dot{q}_j}\right) - \frac{\partial
L}{\partial q_j} = 0
\end{equation}
where $L=T-V$.

\subsection{Problems}
Use Lagrange's equation to solve the 4 problems at the beginning of this
document.

\section{Hamiltonian}
Let's take a look at conservation of energy.  The Lagrangian of a closed system
cannot depend explicitly on time.
Why?
\begin{equation}
\frac{\partial L}{\partial t} = 0
\end{equation}
Show that the total derivative of the Lagrangian for a closed system is:
\begin{equation}
\frac{dL}{dt}=\sum_j \frac{\partial L}{\partial
q}\dot{q}_j+\sum_j\frac{\partial L}{\partial \dot{q}}\ddot{q}
\end{equation}
Using the equation above and Lagrange's equation, show that:
\begin{equation}
\frac{d}{dt}\left(L-\sum_j\dot{q}_j\frac{\partial L}{\partial
\dot{q}_j}\right)=0
\end{equation}

\subsubsection{Answer}
From Lagrange's equation we have
\begin{equation}
\frac{\partial L}{\partial q_j} = \frac{d}{dt}\frac{\partial L}{\partial
\dot{q}_j}
\end{equation}
Plug this into the total derivative
\begin{equation}
\frac{dL}{dt} = \sum_j \dot{q}_j\frac{d}{dt}\frac{\partial L}{\partial
\dot{q}_j}+\sum_j\frac{\partial L}{\partial \dot{q}_j}\ddot{q}_j
\end{equation}
which is equivalent to
\begin{equation}
\frac{dL}{dt} - \sum_j \frac{d}{dt}\left(\dot{q}_j\frac{\partial
L}{\partial \dot{q}_j}\right) = 0
\end{equation}

\vspace{1in}

The quantity in brackets is a constant in time, which we call $-H$.  You can try
to proof the following relation or we will show how to do it later:
\begin{equation}
\sum_l\dot{q}_l\frac{\partial T}{\partial \dot{q}_l} = 2T
\end{equation}
Use this relation and our other relations to show that
\begin{equation}
H=T+V
\end{equation}
$H$ is known as the Hamiltonian.  What is its physical interpretation?

\subsubsection{Answer}
\begin{equation}
\frac{\partial L}{\partial \dot{q}_j} = \frac{\partial(T-U)}{\partial \dot{q}_j}
= \frac{\partial T}{\partial \dot{q}_j}
\end{equation}

\begin{equation}
(T-U) - \sum_j \dot{q}_j\frac{\partial T}{\partial \dot{q}_j} = -H
\end{equation}

\begin{equation}
(T-U) - 2T = - H
\end{equation}

Newest

