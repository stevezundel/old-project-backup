%%This is a very basic article template.
%%There is just one section and two subsections.
\chapter*{Electrostatics}

\section*{Introduction}
You probably already ``know'' several things about electricity and the
introductory, basic concepts leading to electricity that we call electrostatics.
I put know in quotation marks because we often hear physics terms in everyday
language and at least superficially understand what they refer to, but we may
not grasp the full meaning and definition as used in physics. You probably have
heard of electrons, protons, atoms, electricity, charges, electric charge, opposites
attract, likes repel, and potentially other terms.  In the case of opposites
attract and likes repel, we see that physics knowledge has seeped into other
areas of culture.

In this chapter, despite what you may already know, we are going to build a
physical model of electrostatics from the ground up.  We often come to physics
with previous misconceptions because of the way physics is talked about and
sometimes abused in everyday language.  Additionally, sometimes we know
things just because we've been told them, but don't actually know how
they were first learned or understand why we know them.  

The beginning material may seem very familiar or even trivial, but
hopefully by now you have realized that very simple insights in physics
can lead to unexpected and counterintuitive behavior.  It pays to make
sure we are starting off on a solid foundation.  

While performing the experiments below, remember to focus on your
observations and build a model based on your own observations, and not
what you may have previously learned.  We are in part learning about
electrostatics, but we are also learning about model building and how to make
discoveries in science.  After building a model, we can revisit our prior
notions and determine whether they were correct or incorrect.

\subsection*{Questions}
\begin{enumerate}
  \item Briefly list everything your group knows about electricity and things
  related to it.  Save this so we can see later what we knew and didn't know.
\end{enumerate}

\section*{Part 1.0}
The word electricity comes from the Greek ``elektron'', which means amber.  The
Greeks had discovered that when you rub an amber rod, you could use it to pick
up pieces of straw.  We'll try the same experiment using some different
materials.  You have a plastic rod, a glass rod, some wool, and some paper.

\subsection*{Exercise}
\begin{enumerate}
  \item Try to lift bits of paper with the plastic rod and the glass rod, by
  gently touching the pieces of paper with the tip of the rod.
  \item Rub the plastic rod with wool and the glass rod on the paper.  Can
  either be used to pick up bits of paper?  BE
  CAREFUL when rubbing the glass.  Rub gently so that the glass doesn't break in
  your hand.
\end{enumerate}

\subsection*{Questions}
\begin{enumerate}
  \item Based solely on these few observations, what explanations might explain
  why we can pick up the pieces of paper?
  \item Is there any difference in behavior between the glass/paper combo and
  the plastic/wool combo?
  \item Does it make sense at this point to have one explanation for the
  behavior of the rods, or to be open to multiple explanations?
  \item How many different explanations did your group come up with?
\end{enumerate}


\section*{Part 1.1}
Between experiments and after having rubbed the glass and or plastic rods,
touch the rods with your bare hand until they can no longer pick up paper before
preceding to the next section.

\subsection*{Exercise}
\begin{enumerate}
  \item Rub ONE end of a plastic rod with wool and place it on a gimble so that
  it can rotate freely.
  \item Rub one end of a second plastic rod with wool.  Bring the end of this
  second rod close to each of the two ends of the rod on the gimble, but do not let the
  rods touch.
  \item Record your observations.
  \item Do the same thing with two glass rods, but use the paper instead of
  wool.
  \item Repeat the rod and gimble exercise using one glass rod and one plastic
  rod.  Always use the wool with the plastic and the paper with the glass.
\end{enumerate}

\subsection*{Questions}
\begin{enumerate}
  \item Do you notice any new behavior based on your new observations?  Do any
  of the explanations you came up with in the previous section no longer fit
  your observations?
  \item What is your current, best explanation for what you see?
\end{enumerate}


\section*{Part 1.2}
Benjamin Franklin described the phenomenon we saw in part one as two separate
``charges''.  Franklin also introduced the terminology ``positive'' and ``negative''.
His idea was that when rubbed, some objects become positively charged, while
others become negatively charged.  Rubbing causes a transfer of these charges
between two objects.  Franklin assigned the charge given to the plastic rod as
negative and the charge given to the glass rod as positive.

\subsection*{Questions}

\begin{enumerate}
  \item Do Franklin's definitions match your observations?
  \item How do we know that there are two different types of charge?
  \item Using Franklin's terminology, write down a definition of charge and
  incorporate it into your model.
  \item What does it mean for an object to have charge or be charged?
  \item What does it mean for an object to not be charged?  One would
  call this a neutral object.
  \item If the plastic becomes negatively charged when rubbed with wool, what
  happens to the wool?  What about the glass and paper?
\item Draw pictures showing what you think happens with charges when plastic or
glass is rubbed.  Draw pictures for charged and uncharged objects.
\item We learned about conservation laws recently.  What conservation might
apply here?
\end{enumerate}

\subsection*{Homework}
\begin{enumerate}
  \item Summarize in your own words your groups answers to the questions in this
  section.
  \item Write down any questions or misunderstandings you have regarding the
  material covered so far.  
  \item You have seen the attractive and repulsive behavior of the plastic
  and glass rods.  They are clearly exerting forces on each other.  How
  could we test the strength of that force?  What does the force depend
  on?  
  \item Design an experiment to test electric force.  You don't have to be too
  specific with equipment.  For example, you can talk about a charged object
  without specifying the object or how you will charge it.  We are just looking
  for a basic sketch of how you would test the force.  What measurements would
  you take?  How would you plot your data and what would you expect the plot to
  look like?
  \item Thinking back to our studies of gravity, what was the gravitational
  ``charge''?
\end{enumerate}

\section*{Part 2.0}
Remember, when you are directed to rub the plastic and glass, always use wool
for the plastic and paper for the glass.

We have created at least a qualitative model for the behavior of charges and
started thinking about how we could test the force between charged objects.  A
good physical model is capable of making correct predictions.  Our model, if
correct, should be able to predict the behavior of charges in other situations
we haven't observed yet.  When we find that the model fails to correctly predict
new behavior, the model must be modified or replaced with a new, better model.

We have seen the behavior of glass and plastic rods when rubbed with paper or
wool.  Let's predict what would happen if we use a metal spatula on the gimble
instead of one of the two rods from before.

\subsection*{Questions}
\begin{enumerate}
  \item What does your model suggest should happen if you bring a charged
  plastic rod near a metal spatula?
\end{enumerate}

\subsection*{Exercise}
Now check your prediction.
\begin{enumerate}
  \item Place the metal spatula on the gimble.
  \item Rub one end of a plastic rod and bring it near one end of the metal
  spatula, but don't let them touch.  If they do touch, touch the metal spatula
  with your bare hand and start over.
  \item What happens?  What would you expect would happen if you rub a glass rod
  and bring it near the spatula?
  \item Try the glass rod.
  \item Can you ever make the glass or plastic repel the metal spatula?
\end{enumerate}

\subsection*{Questions}
\begin{enumerate}
  \item Do your observations with the spatula fit within the model you have
  created for electrostatics?
  \item If not, how can you change your model to fit the new observations?
  \item Describe what you think is happening with any charges in the metal
  spatula as the glass or plastic rod is brought near.
  \item Draw pictures showing the charges in both objects (i.e. the plastic and
  spatula or the glass and spatula).
\end{enumerate}

\subsection*{Homework}
\begin{enumerate}
  \item Summarize in your own words your groups answers to all the questions
  above.
  \item Write down any questions or misunderstanding you have regarding the
  material covered so far.
\end{enumerate}


\section*{Part 3.0}
For the next part, you will need a balloon and some scraps of paper.  

\subsection*{Exercise}
\begin{enumerate}
  \item Charge a balloon by rubbing it on your clothing or the wool.
\end{enumerate}

\subsection*{Questions}
\begin{enumerate}
  \item How do you know the balloon is charged?
\end{enumerate}


\subsection*{Exercise}
\begin{enumerate}
  \item See if your charged balloon will stick to the wall.
  \item Now see if your charged balloon can pick up scraps of paper like our
  charged rods did in Part 1.
\end{enumerate}

\subsection*{Questions}
\begin{enumerate}
  \item How long do you think the balloon would stay attached to the wall?
  \item Does your model explain why the balloon picks up pieces of paper, and if
  not, how could it be modified?
  \item Draw a picture of the charges in the balloon and in the paper that
  explains why the paper is attracted to the balloon.
\end{enumerate}     

\subsection*{Homework}
\begin{enumerate}
  \item Summarize in your own words your groups answers to the questions in this
  section.
  \item Write down any questions or misunderstandings you have regarding the
  material covered so far.
  \item Check your group's understanding of the balloon and wall using this
  simulation:
  \url{http://phet.colorado.edu/sims/html/balloons-and-static-electricity/latest/balloons-and-static-electricity_en.html}
  \item FYP 10.2
  \item FYP 10.3
\end{enumerate}

\section*{Part 4.0}
\subsection*{How Different Materials Behave with Charge}
We have found that rubbing objects together can create charge on them.  We'll
work with tape in this section.  An easy way to ``rub'' tape against another
surface is to stick the tape on the table and rip it off.  Hint, it's easier to
work with the tape if you create tabs at the two ends by folding the end of the
tape over on itself.

\subsection*{Exercise - Tape}
\begin{enumerate}
  \item Charge a piece of tape.
  \item Can you tell which type of charge it has?
  \item Bonus brain teaser: Can you create a piece of tape with the opposite
  charge?
\end{enumerate}
 
\subsection*{Exercise - Metallic Sphere}   
I'll explain how to charge a metal sphere, and we'll investigate why this works
later.  Get a Styrofoam plate from the supplies bench.  Rub the top of the
Styrofoam with the wool until you hear some sparks.  Whiling touching the metal
sphere with your hand, bring it close to the Styrofoam, but do not touch the
sphere to the Styrofoam.  Remove your hand from the metal part of the sphere,
then move the sphere away from the styrofoam.  How can you test whether the
sphere is charged or not?

\subsection*{Removing Charge}
Discharge HALF of a piece of tape by running
your finger along half of the tape repeatedly.  Check to see that the tape is
discharged.  Is the other half still charged?  Try to discharge half of the
sphere.  Were you successful?  Why or why not?


We call these two types of materials insulators and conductors.  Write down a
definition for each based on your observations.

\subsection*{Questions}
\begin{enumerate}
  \item We say an object is grounded if it has a connection to the ground through
conductors or at least weakly insulating materials.  How did grounding play a
role in this section?
\item How does our method of charging work?
\item How is this method fundamentally different then rubbing objects together
like we did with the glass and plastic rods?
\end{enumerate}


\subsection*{Homework}
\begin{enumerate}
  \item Summarize in your own words your groups answers to the questions in this
  section.
  \item Write down any questions or misunderstandings you have regarding the
  material covered so far.
  \item How many different ways have we discovered to charge an object?  Explain
  each.
\end{enumerate}

\section*{Part 5.0 - Take your model for a spin - Electroscope}

We have quite a few pieces in our model now.  Let's see if we can use it to
predict the behavior of a device called an electroscope.  Use your model to
predict what will happen in question FYP 10.1.  The two ``needles'' in the
pictures and the circle at the bottom are all metal and are all connected (i.e.
they form a connected conductor).  Check with me if you are having trouble
understanding what the picture is showing.  Answer the question using your model
and then check with me.

\subsection*{Exercise}
Now check your answer to FYP 10.1 using an electroscope.  Be careful when
handling the electroscope.  It has a sensitive needle.

\subsection*{Homework}
\begin{enumerate}
  \item A light strip of aluminum foil is draped over a horizontal wooden pencil.  When
a rod carrying a positive charge is brought close to the foil, the two parts of
the foil stand apart.  Why?  What kind of charge is on the foil?  Draw a
picture of the situation and the charge distributions.
od.  After it touches the rod, the sphere is repelled by the
rod.  Explain.
   \item FYP 10.7
   \item FYP 10.9 
\end{enumerate}


\section*{Part 6.0 - Electric Force}
We have seen how charged objects will react to each other.  One charged object
can change the motion of another, which, as we know from Newton's laws, means
the two objects are exerting forces on each other.  

\begin{enumerate}
  \item What physical quantities does the electric force depend on?
\end{enumerate}

\section*{Part 7.0 - Fields}
A field is a powerful and useful concept in physics.  A field defines a quantity
at all points in space.  For example, a temperature field assigns a temperature
to all points in space.  Consider the Sun, with no planets orbiting around it
for now.  Pick a few locations surrounding the Sun and draw vectors of the
gravitational force an object would feel per unit mass at your chosen locations.
The diagram below is an example of how to set up your drawing.

\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_Sun1.jpg} 
\end{figure}


These vectors
represent the gravitational field at that location.
Starting at the Sun, draw lines from the Sun that pass through each of the vectors you
drew above.  Draw the lines such that at any point along the line, the tangent
to the line is an appropriate gravitational force vector.  These are called
field lines.  


Now make a new drawing the includes the Earth and the Sun, as in the figure
below.  Repeat the process above.
Draw gravitional field vectors for the Earth and the Sun at each point, and
then draw the resultant vector at each point.



\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_Sun2.jpg} 
\end{figure}

Know try to draw field lines for the various charge arrangements below.  How is
this different or similiar to the gravitational examples?


\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_Negative.jpg} 
\end{figure}

\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_Positive.jpg} 
\end{figure}

\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_PosNeg.jpg} 
\end{figure}

\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_PosPos.jpg} 
\end{figure}

\begin{figure}[htbp]
   \centering
   \includegraphics[width=3.3in] {Tutorials/images/Field_Lines_Triple.jpg} 
\end{figure}


\begin{comment}
\item Challenge problem:
Calculate the number of electrons in a small, electrically neutral silver pin
that has a mass of \SI{10.0}{g}.  Silver has \num{47} electrons per atom, and
its molar mass is \SI{107.87}{g/mol}.  Electrons are added to the pin until the
net negative charge is \SI{1.00}{mC}.  How many electrons are added for every
\num{e9} electrons already present.
\item Hospital personnel must wear special conducting shoes while working around
oxygen in an operating room.  Why?  Contrast with what might happen if people
wore rubber soled shoes.
\item A light, uncharged metallic sphere suspended from a thread is attracted to a
charged rubber r
\end{comment}
